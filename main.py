
# function of a healthy person
def biggest_path(x: dict) -> str:
    def inner(d: dict) -> list:
        result = []
        for k, v in d.items():
            if isinstance(v, dict):
                result += [f'/{k}'] + list(map(lambda n: f'/{k}{n}', inner(v)))
        return result

    paths = ['/'] + inner(x)
    return max(paths)


# function with restrictions
def biggest_validated_path(x: dict) -> str:
    def check_duplicates(d: list) -> bool:
        return len(d) == len(set(d))

    def check_name(name: str) -> bool:
        return all(c.isdigit() or ('a' < c < 'z') or ('A' < c < 'Z') for c in name)

    def inner(d: dict) -> list:
        result = []
        for k, v in d.items():
            if not check_name(k):
                raise ValueError(f'incorrect name: {k!r}')
            if isinstance(v, list):
                if not check_duplicates(v):
                    raise ValueError(f'duplicates found {v}')
                for file_name in v:
                    if not check_name(file_name):
                        raise ValueError(f'incorrect name: {file_name!r}')
            if isinstance(v, dict):
                result += [f'/{k}'] + list(map(lambda n: f'/{k}{n}', inner(v)))
        return result

    paths = ['/'] + inner(x)
    return max([p for p in paths if len(p) < 256])


if __name__ == '__main__':
    d1 = {'dir1': {}, 'dir2': ['file1'], 'dir3': {'dir4': ['file2'], 'dir5': {'dir6': {'dir7': {}}}}}
    d2 = {'dir1': ['file1', 'file1']}
    d3 = {'أرض الإسلام': ['file1', 'file2']}

    for d in [d1, d2, d3]:
        print(biggest_path(d))

    for d in [d1, d2, d3]:
        try:
            print(biggest_validated_path(d))
        except ValueError as e:
            print(e)
